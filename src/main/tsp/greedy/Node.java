package tsp.greedy;

import java.util.ArrayList;

/**
 * Created by alina.skorokhodova@vistar.su on 13.11.2017.
 */
public class Node {

    public String currentCity;
    public int coast;  //pathcost
    public ArrayList<String> ancestorList = new ArrayList<>();

    public Node(String nodeName, int pathCost, ArrayList<String> aList) {
        currentCity = nodeName;
        coast = pathCost;
        ancestorList.addAll(aList);
    }
}
