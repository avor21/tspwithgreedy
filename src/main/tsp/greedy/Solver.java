package tsp.greedy;

import java.io.*;

/**
 * Created by alina.skorokhodova@vistar.su on 13.11.2017.
 */
public class Solver {

    public static void main(String[] args) throws IOException {

        String outputPathLog = "src\\resource\\output_log.txt"; //FixMe: add path to yml properties
        new GreedyAlgorithm(new VerticesConstructor().createNewConstructor(), outputPathLog).greedy();
    }
}
