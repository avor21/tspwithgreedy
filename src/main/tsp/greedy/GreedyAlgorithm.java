package tsp.greedy;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * Created by alina.skorokhodova@vistar.su on 27.11.2017.
 */
public class GreedyAlgorithm {

    private final VerticesConstructor vc;
    private final String outputPathLog;

    public GreedyAlgorithm(VerticesConstructor constructor, String outputPathLog) {
        this.vc = constructor;
        this.outputPathLog = outputPathLog;
    }

    public void greedy() throws FileNotFoundException, UnsupportedEncodingException {
        Queue<Node> greedyq = new LinkedList<>();
        Queue<Node> greedytraversal = new LinkedList<>();
        ArrayList<String> aList = new ArrayList<>();

        String startNode = vc.getStartCity();
        Integer numberOfCities = vc.getNumberOfCities();
        Map<String,Integer> nodeDictionary = vc.getNodeDictionary();
        int[][] disMatrix = vc.getCoastMatrix();

        Node srcNode = new Node(startNode, 0, aList);
        Node lastNode;
        greedyq.add(srcNode);
        greedytraversal.add(srcNode);
        int totalCost, returnHome;
        while (greedytraversal.size() != numberOfCities) {
            int min = Integer.MAX_VALUE;
            srcNode = greedyq.peek();
            int nextNode = 0;
            int src = nodeDictionary.get(srcNode.currentCity);
            for (int j = 0; j < numberOfCities; j++) {
                if (!srcNode.ancestorList.contains(getKeyInDictionaryByValue(nodeDictionary, j))) {
                    if (j != src) {
                        if (disMatrix[src][j] < min) {
                            min = disMatrix[src][j];
                            nextNode = j;
                        }
                    }
                }
            }
            String dest = getKeyInDictionaryByValue(nodeDictionary, nextNode);
            if (!srcNode.ancestorList.contains(dest)) {
                aList.addAll(srcNode.ancestorList);
                aList.add(srcNode.currentCity);
                totalCost = srcNode.coast + min;
                Node destNode = new Node(dest, totalCost, aList);
                greedyq.add(destNode);
                greedytraversal.add(destNode);
                aList.clear();
            }
            greedyq.remove();
        }
        lastNode = greedyq.peek();
        int ln = nodeDictionary.get(lastNode.currentCity);
        returnHome = disMatrix[ln][nodeDictionary.get(startNode)];
        totalCost = lastNode.coast + returnHome;
        aList.addAll(lastNode.ancestorList);
        aList.add(lastNode.currentCity);
        Node last = new Node(startNode, totalCost, aList);
        greedytraversal.add(last);
        PrintWriter writerLog = new PrintWriter(outputPathLog, "UTF-8");
        System.out.print("\nTotal path: " + greedytraversal.peek().currentCity + " ");
        greedytraversal.remove();
        while (!greedytraversal.isEmpty()) {
            Node head = greedytraversal.peek();
            java.util.Iterator<String> itr = head.ancestorList.iterator();
            StringBuilder path = new StringBuilder();
            while (itr.hasNext()) {
                path.append(itr.next());
            }
            path.append(head.currentCity);
            System.out.print(head.currentCity + " ");
            writerLog.println(path + ", " + head.coast + ", ");
            greedytraversal.remove();
            totalCost = head.coast;
        }
        System.out.println("\nTotal cost: " + totalCost);
        writerLog.close();
    }

    private String getKeyInDictionaryByValue(Map<String,Integer> nodeDictionary, int value) {
        for (Map.Entry<String, Integer> entry : nodeDictionary.entrySet()) {
            if (entry.getValue() == value)
                return entry.getKey();
        }
        return null;
    }
}
