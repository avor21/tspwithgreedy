package tsp.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by alina.skorokhodova@vistar.su on 27.11.2017.
 */
public class VerticesConstructor {

    private static final BufferedReader BUFFER = new BufferedReader(new InputStreamReader(System.in));
    private static final char START_SYMBOL = 'a';
    private static final char END_SYMBOL = 'z';

    private Integer numberOfCities;
    private String startCity;

    private Map<String, Integer> nodeDictionary; //FixMe: Rename me pls!
    private int[][] coastMatrix;

    public VerticesConstructor createNewConstructor() {
        numberOfCities = readNumberOfCitiesFromConsole();
        coastMatrix = generateRandomCoastMatrix(numberOfCities);
        printMatrix(coastMatrix);
        nodeDictionary = fillNodeDictionary(START_SYMBOL, END_SYMBOL, numberOfCities);
        printDictionary(nodeDictionary);
        startCity = readStartCityFromConsole();
        return this;
    }

    public Integer getNumberOfCities() {
        return numberOfCities;
    }

    public Map<String, Integer> getNodeDictionary() {
        return nodeDictionary;
    }

    public String getStartCity() {
        return startCity;
    }

    public int[][] getCoastMatrix() {
        return coastMatrix;
    }

    private Integer readNumberOfCitiesFromConsole() {
        String value = null;
        try {
            System.out.print("Enter number of cities: ");
            value = BUFFER.readLine();
        } catch (IOException ex) {
            System.err.println("ERROR! Can't read input value from console!:( \n" + ex);
        } finally {
            if (value == null)
                throw new IllegalArgumentException("'numberOfCities' can't be NULL !");
        }
        return Integer.parseInt(value);
    }

    private String readStartCityFromConsole() {
        String value = null;
        try {
            System.out.print("\nEnter start city: ");
            value = BUFFER.readLine();
        } catch (IOException ex) {
            System.err.println("ERROR! Can't read input value from console!:( \n" + ex);
        } finally {
            try {
                BUFFER.close(); //удалить при добавлении второго метода!
            } catch (IOException e) {
                System.err.println("ERROR! When I try close BUFFER!:( \n" + e);
            }
        }

        if (value == null || value.equals(""))
            throw new IllegalArgumentException("'startCity' can't be NULL or empty!");

        return value;
    }

    private int[][] generateRandomCoastMatrix(int size) {
        int[][] matrix = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                matrix[i] = new Random()
                        .ints(1, 50)
                        .limit(size)
                        .toArray();
        }

        return makeNullOnDiagonal(matrix);
    }

    private int[][] makeNullOnDiagonal(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++)
                if (i == j) matrix[i][j] = 0;
        }

        return matrix;
    }

    private Map<String, Integer> fillNodeDictionary(char start, char end, int size) {
        Map<String, Integer> map = new HashMap<>();
        String[] letters = IntStream.rangeClosed(start, end).limit(size)
                .mapToObj(c -> "" + (char) c).toArray(String[]::new);

        for (int i = 0; i < size; i++) {
            map.put(letters[i], i);
        }

        return map;
    }

    private void printDictionary(Map<String, Integer> map) {
        System.out.print("\nYour cities: ");
        for (Map.Entry<String, Integer> entry : nodeDictionary.entrySet()) {
            System.out.print(entry.getKey() + ", ");
        }
    }

    private static void printMatrix(int[][] matrix) {
        System.out.println("Your coast matrix: ");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
