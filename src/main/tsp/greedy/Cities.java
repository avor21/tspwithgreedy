package tsp.greedy;

/**
 * Created by alina.skorokhodova@vistar.su on 13.11.2017.
 */
public enum Cities {

    ABRAMOVKA("Абрамовка"), ABROSIMOVO("Абросимово"), AIDAROVO("Айдарово"), ALEINIKOVO("Алениково"), ALEKSEEVKA("Алексеевка"),
    ALOE_POLE("Алое поле"), ANDRIANOVKA("Адриановка"), ANDRUSHEVKA("Андрюшевка"), ANNA("Анна"), ANNOVKA("Анновка"), ANOKHINKA("Анохинка"),
    ANOSHKINO("Аношкино"), ARTUSHKINO("Артюшкино"), ARKHIPOVKA("Архиповка"), BABINO("Бабино"), BABKA("Бабка"), BABYAKOVO("Бабяково"),
    BANNOE("Банное"), BASOVKA("Басовка"), BELOGORIE("Белогорье"), BEREZKI("Березки"), BEREZNYAGI("Березняги"), BEREZOVKA("Березовка"),
    BIRUCH("Бирюч"), BOBROV("Бобров"), BOGANA("Богана"), BOGOSLOVKA("Богословка"), BOGUCHAR("Богучар"), BODEEVKA("Бодеевка"), BOEVO("Боево"),
    BOLDIREVKA("Болдыревка"), BOLSHIE_YASIRKI("Большие Ясырки"), BOR("Бор"), BONDAREVO("Бондарево"), BORISOGLEBSK("Борисоглебск"),
    BORSHEVO("Борщево"), BRATKI("Братки"), BRODOVOE("Бродовое"), BURAVCOVKA("Буравковка"), BURLYAEVKA("Бурляевка"), BUTIRKI("Бутырки"),
    BICHOK("Бичок"), VALENTINOVKA("Валентиновка"), VITEBSK("Витебск"), VOLYA("Воля"), VORONEZH("Воронеж"),
    VOSKHOD("Восход"), VYAZOVKA("Вязовка"), GALIEVKA("Галиевка"), GNILOE("Гнилое"), GORELKA("Горелка"), GORKI("Горки"), GREMYACHIE("Гремячье"),
    DAVIDOVKA("Давыдовка"), DEVICA("Девица"), DOLGOE("Долгое"), DOLINA("Долина"), DRUZHBA("Дружба"), ELENOVKA("Еленовка"), ERMOLOVKA("Ермоловка"),
    ESIPOVO("Есипово"), ZHELANNOE("Желанное"), ZARECHIE("Заречье"), KAZANKA("Казанка"), KALACH("Калач"), KANTEMIROVKA("Кантемировка"),
    KVASHINO("Квашино"), KNYAZEVO("Князево"), MANINO("Манино"), NOVOKHOPERSK("Новохоперск"), ORLOVO("Орлово"), OSTROVKI("Островки"),
    OSTROGOZHSK("Острогожск"), PANINO("Панино"), PETROVKA("Петровка"), PIROGOVO("Пирогово"), REPIEVKA("Репьевка"), ROMANOVKA("Романовка"),
    ROSSOSH("Россошь"), RUDKINO("Рудкино"), SELYAVNOE("Селявное"), SEMILUKI("Семилуки"), SINYAVKA("Синявка"), SKRIPNIKOVO("Скрипниково"),
    STUPINO("Ступино"), TATARINO("Татарино"), TIMIRAZEVO("Тимирязево"), UGLYANEC("Углянец"), KHLEBNOE("Хлебное"), SHUBNOE("Шубное"), ERTIL("Эртиль"),
    UZHNOE("Южное"), YABLOCHNOE("Яблочное"), YASENKI("Ясенки"), YACHEIKA("Ячейка");

    Cities(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
